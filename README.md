# Crates Mirror
This script is meant to download all crates.io crates for use
in an air-gapped environment.

## How
It iterates over the [crates.io-index](https://github.com/rust-lang/crates.io-index), downloading each package to its proper path.
You must also mirror the index repository, and change the config.json file there to point to your crates server.

## Usage
```bash
git clone https://github.com/rust-lang/crates.io-index
mkdir mirror; cd mirror
python ../main.py ../crates.io-index  # If you want to save some time on redirections, see if you want to change a line in the script
```