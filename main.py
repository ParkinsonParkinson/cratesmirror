#!/usr/bin/env python3
import json
import os
import sys

import requests_futures.sessions

CRATE_RESULT_PATH_FORMAT = "api/v1/crates/{crate}/{version}/download"

# This is the URL used in the crates.io-index config.json
# However, it mostly redirects to the static URL below.
# Use the static URL to avoid redirects (should be faster), but keep in mind it might not be as airtight as this one.
# If you use the static URL, also change the commented `futures.append` line below
CRATE_DOWNLOAD_URL_FORMAT = "https://crates.io/{path}"
# CRATE_DOWNLOAD_URL_FORMAT = "https://static.crates.io/crates/{crate}/{crate}-{version}.crate"


def write_response_factory(path):
    def write_response(response, *args, **kwargs):
        if response.status_code != 200:
            print("GET for '{}' returned status {}".format(path, response.status_code), file=sys.stderr)
        else:
            os.makedirs(os.path.dirname(path), exist_ok=True)
            with open(path, 'wb') as out_file:
                out_file.write(response.content)
            print("Finished path {}".format(path))
        return response

    return write_response


def mirror_crates(index_dir):
    exception_counter = 0
    futures = []

    session = requests_futures.sessions.FuturesSession(max_workers=16)

    for dirpath, directories, files in os.walk(index_dir):
        for filename in files:
            if filename == "config.json" or os.path.join(index_dir, ".git") in dirpath:
                continue  # Yikes
            try:
                with open(os.path.join(dirpath, filename)) as file:
                    for line in file.readlines():
                        j = json.loads(line)
                        path = CRATE_RESULT_PATH_FORMAT.format(crate=j["name"], version=j["vers"])
                        if os.path.exists(path):
                            # This version is already downloaded, skip it
                            continue
                        # Use this line for the static URL
                        #futures.append(session.get(CRATE_DOWNLOAD_URL_FORMAT.format(crate=j["name"], version=j["vers"]),

                        # Use this line for the main crates URL
                        futures.append(session.get(CRATE_DOWNLOAD_URL_FORMAT.format(path=path),

                                                   # Use this line for both :)
                                                   hooks={"response": write_response_factory(path)}))
            except Exception as e:
                exception_counter += 1
                if exception_counter > 50:
                    raise
                print("File '{}' failed: {}".format(os.path.join(dirpath, filename), e), file=sys.stderr)

    for future in futures:
        future.result()


if __name__ == '__main__':
    if len(sys.argv) < 2:
        print("Usage: {} <crates.io-index dir>".format(sys.argv[0]), file=sys.stderr)
        exit(1)
    exit(mirror_crates(sys.argv[2]))
